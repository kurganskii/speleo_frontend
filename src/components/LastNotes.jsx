import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {RichText} from 'prismic-reactjs';
import {NavLink} from 'react-router-dom'
import {Grid} from "react-bootstrap";

class LastNotes extends Component {

    componentDidMount() {
        this.props.loadData()
    }

    render() {
        const {articles} = this.props
        return <Grid fluid className="last-story">
            <h2 className="last-story-title">Путевые заметки</h2>
            {articles.map((aricle, index) =>
                <NavLink key={index} to={`/articles/${aricle.id}`}
                         className="last-story-item">{RichText.asText(aricle.data.title)}</NavLink>
            )}
        </Grid>;
    }
}

LastNotes.propTypes = {
    articles: PropTypes.array.isRequired,
    loadData: PropTypes.func.isRequired
};

export default LastNotes;