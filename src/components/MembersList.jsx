import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Member from './Member'
import {Grid, Col} from 'react-bootstrap';

class Members extends Component {
    render() {
        const {members} = this.props;
        return (
            <Grid fluid className="member-list">
                {members.map((m, index) => <Col md={3}>
                    <Member key={index} name={m.name} image={'url' in m.avatar ? m.avatar : m.avatar_2}/>
                </Col>)}
            </Grid>)

    }
}

Members.propTypes = {
    members: PropTypes.array.isRequired,
}

Members.defaultProps = {}

export default Members
