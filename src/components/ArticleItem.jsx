import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {NavLink} from 'react-router-dom'
import {RichText} from 'prismic-reactjs';
import Moment from 'react-moment'
import {Grid, Col, Image} from 'react-bootstrap';

class ArticleItem extends Component {

    render() {
        const {article} = this.props;
        const data = article.data;
        let description;
        const preview = article.data.body.map(function (slice, index) {
            if (slice.slice_type === 'preview') {
                let image = '<div/>';
                if ('url' in slice.primary.image_preview) {
                    image = <Image responsive src={slice.primary.image_preview.url}/>
                }
                description = <div className="feed-description"> {RichText.render(slice.primary.preview)} </div>
                return (
                    <div className="preview-img" key={index}>
                        {image}
                    </div>
                );
            } else {
                return <div/>
            }
        });
        return (
            <Col md={4} sm={6} className="feed-item-container">
                <NavLink to={`/articles/${article.id}`}>
                    <Grid fluid className="feed-item">
                        {preview}
                        <h4 className="feed-title">{RichText.asText(data.title)}</h4>
                        {description}
                        <p className="feed-date"><Moment parse="YYYY-MM-DD" locale="ru"
                                                         format="DD MMMM">{data.date}</Moment></p>
                    </Grid>
                </NavLink>
            </Col>
        )
    }
}

ArticleItem.propTypes = {
    article: PropTypes.object.isRequired,
};

ArticleItem.defaultProps = {};

export default ArticleItem
