import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {RichText} from 'prismic-reactjs';
import {Grid, Row} from 'react-bootstrap';
import SliceRenderer from "./SliceRenderer";


class ArticleView extends Component {
    componentDidMount() {
        this.props.loadArticle();
    }

    render() {
        const {article} = this.props;
        if (article === undefined) {
            return (<div/>);
        }
        const {title} = article.data;
        return (
            <Grid fluid={true} className="article">
                <Row>
                    <div className="article-title">{RichText.asText(title)}</div>
                </Row>
                <Row className="article-text">
                    <SliceRenderer slice={article.data}/>
                </Row>
            </Grid>
        );
    }
}

ArticleView.propTypes = {
    loadArticle: PropTypes.func.isRequired,
    article: PropTypes.object
};

export default ArticleView;