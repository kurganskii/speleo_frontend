import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap';

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <Grid fluid className="footer-overlay">
                    <Row>
                        <div className="footer-title">Собрания проходят каждый четверг в 18:40 в 16 корпусе (бывш.
                            ДК СГАУ)
                        </div>
                    </Row>

                    <Row sm={12} className="social-group">
                        <img src="/media/vk.png" alt="vk" className="social-icon"/>
                        <img src='/media/instagram.png' alt="instagramm" className="social-icon"/>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default Footer;