import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Row, Grid} from 'react-bootstrap'
import ArticleItem from './ArticleItem'

class ArticlesList extends Component {

    componentWillMount() {
        this.props.dispatchCallGetAllArticles();
    }

    render() {
        return (
            <div>
                <Grid fluid={true}>
                    <h2 className="news-title"> {this.props.title} </h2>
                    <Grid fluid={true}>
                        <Row className="row-flex">
                            {this.props.articles.map((article, index) =>
                                <ArticleItem key={index} tags={this.props.tags} article={article}
                                             isAdmin={this.props.isAdmin}/>
                            )}
                        </Row>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

ArticlesList.propTypes = {
    dispatchCallGetAllArticles: PropTypes.func.isRequired,
    loadTags: PropTypes.func.isRequired
};

export default ArticlesList;