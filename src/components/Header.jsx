import React from 'react';
import { Grid } from 'react-bootstrap';

class Header extends React.Component {
  render() {

    return (
      <div className="header">
        <Grid fluid={true} className="header-overlay">
          <h1 className="title-header">Спелеоклуб Самарского Университета</h1>
        </Grid>
      </div>
    );
  }
}

export default Header;