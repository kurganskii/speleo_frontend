import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Grid, Col, Row} from 'react-bootstrap';
import {RichText} from 'prismic-reactjs';
import Moment from "react-moment";


class Report extends Component {
    render() {
        const {url, date, title} = this.props;
        return <Grid fluid>
            <Row className="report">

                {RichText.asText(title)}
                <a href={url}>
                    скачать
                </a>
                <Moment parse="YYYY-MM-DD" locale="ru"
                        format="(MMM YYYY)">{`${date}`}</Moment>
            </Row>
        </Grid>
    }
}

Report.propTypes = {
    url: PropTypes.string.isRequired,
    date: PropTypes.object.isRequired,
    title: PropTypes.array.isRequired,
};

Report.defaultProps = {};

export default Report
