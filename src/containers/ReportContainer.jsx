import { connect } from 'react-redux'
import ArticleView from '../components/ArticleView';
import { callGetAllArticlesByTag } from '../actions/ArticleAction'
import { getReportArticle } from '../selectors/ArticleSelector'

const mapStateToProps = (state, props) => {
    let articles = getReportArticle(state, props);
    if (articles.length > 0) {
        return { article: articles[0] }
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loadArticle: () => {
            dispatch(callGetAllArticlesByTag('reports'))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleView)