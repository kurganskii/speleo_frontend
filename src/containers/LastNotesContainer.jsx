import { connect } from 'react-redux'
import LastNotes from '../components/LastNotes'
import { callGetLastNotes } from '../actions/ArticleAction'
import { getLastNoteArticles } from '../selectors/ArticleSelector.js'


const mapStateToProps = (state, props) => {
    return {
        articles: getLastNoteArticles(state, props)
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loadData: () => {
            dispatch(callGetLastNotes())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LastNotes)