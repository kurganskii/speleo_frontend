import { connect } from 'react-redux'
import ArticleView from '../components/ArticleView';
import { callGetAllArticlesByTag } from '../actions/ArticleAction'
import { filterArticlesByTag } from '../selectors/ArticleSelector'

const mapStateToProps = (state, props) => {
    let articles = filterArticlesByTag(state, props, 'about_us');
    if (articles.length > 0) {
        return { article: articles[0] }
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loadArticle: () => {
            dispatch(callGetAllArticlesByTag('about_us'))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleView)