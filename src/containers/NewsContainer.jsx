import { connect } from 'react-redux'
import ArticlesList from '../components/ArticlesList'
import { callGetAllArticlesByTag } from '../actions/ArticleAction'
import { getAllNews } from '../selectors/ArticleSelector.js'

const mapStateToProps = (state, props) => {
    return {
        title: "Все новости",
        articles: getAllNews(state),
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        dispatchCallGetAllArticles: () => {
            dispatch(callGetAllArticlesByTag('news'))
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(ArticlesList)