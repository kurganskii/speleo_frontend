import { Switch, Route } from 'react-router-dom';
import React, { Component } from 'react';
import axios from 'axios'
import { Grid, Col, Row } from 'react-bootstrap';
import Footer from '../components/Footer'
import Header from '../components/Header';

import LastNotesContainer from './LastNotesContainer'
import NewsContainer from './NewsContainer'
import ArticleViewContainer from './ArticleViewContainer'
import AboutContainer from './AboutContainer'
import NavigationContainer from './NavigationContainer'
import TravelsContainer from './TravelsContainer'
import ReportContainer from "./ReportContainer";


class App extends Component {
  constructor(props) {
    super(props)
    axios.defaults.baseURL = "/api";
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    axios.defaults.headers.post['Authorization'] = localStorage.getItem('token') 
    axios.defaults.headers.get['Authorization'] = localStorage.getItem('token') 
  }

  render() {
    return (
      <div>
        <NavigationContainer/>
        <Header />
        <Grid className='content'>
          <Row className="show-grid">
            <Col md={9}>
              <Switch basename={process.env.PUBLIC_URL}>
                <Route exact path='/' component={NewsContainer} />
                <Route path="/news" component={NewsContainer} />
                <Route path="/about" component={AboutContainer} />
                <Route path="/travels" component={TravelsContainer} />
                <Route path="/reports" component={ReportContainer} />
                <Route path="/articles/:id" component={ArticleViewContainer} />
              </Switch>
            </Col>
            <Col md={3}>
              <LastNotesContainer />
            </Col>
          </Row>
        </Grid>
        <Footer />
      </div>
    );
  }
}


export default App;
