import Prismic from 'prismic-javascript';
import * as types from '../constants/ActionTypes';
/*
 * action creators
 */

function addArticles(data) {
    return {
        type: types.ARTICLE_ADD_ALL,
        data
    };
}

function getArticle(id, data) {
    return {
        type: types.ARTICLE_GET_ONE,
        id,
        data
    };
}

const apiEndpoint = 'https://speleossau.prismic.io/api/v2';

export function callGetAllArticles() {
    return dispatch => {

        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.type', 'article'), { orderings: '[my.article.date desc]' }).then(response => {
                if (response) {
                    dispatch(addArticles(response.results));
                }
            });
        });
    }
}

export function callGetArticleById(id) {
    return dispatch => {
        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.id', id)).then(function (response) {
                dispatch(getArticle(id, response.results[0]))
            });
        });
    }
}

export function callGetAllArticlesByTag(tag) {
    return dispatch => {
        Prismic.api(apiEndpoint).then(api => {
            api.query(Prismic.Predicates.at('document.tags', [tag]), { orderings: '[my.article.date desc]' }).then(function (response) {
                dispatch(addArticles(response.results))
            });
        });
    }
}

export function callGetLastNotes(data) {
    return callGetAllArticlesByTag('notes')
}