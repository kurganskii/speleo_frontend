/* eslint-disable no-mixed-operators */
import React from 'react';
import {RichText} from 'prismic-reactjs';

const Elements = RichText.Elements;

// -- Function to create a unique key ID
const uuid = function (doc) {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
};

// -- Function to add unique key to props
const propsWithUniqueKey = function (props) {
    return Object.assign(props || {}, {key: uuid()});
};
export const linkResolver = function (link) {

};
// -- HTML Serializer
export const htmlSerializer = function (element, content, children) {

    var props = {};

    switch (element.type) {

        case Elements.heading1: // Heading 1
            return React.createElement('h1', propsWithUniqueKey(props), children);

        case Elements.heading2: // Heading 2
            return React.createElement('h2', propsWithUniqueKey(props), children);

        case Elements.heading3: // Heading 3
            return React.createElement('h3', propsWithUniqueKey(props), children);

        case Elements.heading4: // Heading 4
            return React.createElement('h4', propsWithUniqueKey(props), children);

        case Elements.heading5: // Heading 5
            return React.createElement('h5', propsWithUniqueKey(props), children);

        case Elements.heading6: // Heading 6
            return React.createElement('h6', propsWithUniqueKey(props), children);

        case Elements.paragraph: // Paragraph
            return React.createElement('p', propsWithUniqueKey(props), children);

        case Elements.preformatted: // Preformatted
            return React.createElement('pre', propsWithUniqueKey(props), children);

        case Elements.strong: // Strong
            return React.createElement('strong', propsWithUniqueKey(props), children);

        case Elements.em: // Emphasis
            return React.createElement('em', propsWithUniqueKey(props), children);

        case Elements.listItem: // Unordered List Item
            return React.createElement('li', propsWithUniqueKey(props), children);

        case Elements.oListItem: // Ordered List Item
            return React.createElement('li', propsWithUniqueKey(props), children);

        case Elements.list: // Unordered List
            return React.createElement('ul', propsWithUniqueKey(props), children);

        case Elements.oList: // Ordered List
            return React.createElement('ol', propsWithUniqueKey(props), children);

        case Elements.image: // Image
            const linkUrl = element.linkTo ? element.linkTo.url || linkResolver(element.linkTo) : null;
            const linkTarget = (element.linkTo && element.linkTo.target) ? {target: element.linkTo.target} : {};
            const linkRel = linkTarget.target ? {rel: 'noopener'} : {};
            const img = React.createElement('img', {
                src: element.url,
                alt: element.alt || '',
                className: 'img-responsive'
            });
            return React.createElement(
                'p',
                propsWithUniqueKey({className: [element.label || '', 'block-img'].join(' ')}),
                linkUrl ? React.createElement('a', Object.assign({href: linkUrl}, linkTarget, linkRel), img) : img
            );

        case Elements.embed: // Embed
            props = Object.assign({
                "data-oembed": element.oembed.embed_url,
                "data-oembed-type": element.oembed.type,
                "data-oembed-provider": element.oembed.provider_name,
            }, element.label ? {className: element.label} : {});
            const embedHtml = React.createElement('div', {dangerouslySetInnerHTML: {__html: element.oembed.html}});
            return React.createElement('div', propsWithUniqueKey(props), embedHtml);

        case Elements.hyperlink: // Image
            const targetAttr = element.data.target ? {target: element.data.target} : {};
            const relAttr = element.data.target ? {rel: 'noopener'} : {};
            props = Object.assign({
                href: element.data.url || linkResolver(element.data)
            }, targetAttr, relAttr);
            return React.createElement('a', propsWithUniqueKey(props), children);

        case Elements.label: // Label
            props = element.data ? Object.assign({}, {className: element.data.label}) : {};
            return React.createElement('span', propsWithUniqueKey(props), children);

        case Elements.span: // Span
            if (content) {
                return content.split("\n").reduce((acc, p) => {
                    if (acc.length === 0) {
                        return [p];
                    } else {
                        const br = React.createElement('br', propsWithUniqueKey());
                        return acc.concat([br, p]);
                    }
                }, []);
            } else {
                return null;
            }

        default: // Always include a default that returns null
            return null;
    }
};